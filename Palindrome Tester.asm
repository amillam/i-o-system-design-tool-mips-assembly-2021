# For part 2 of System Design Tools and MIPS Assembly
# Purpose of this program: Write a program that will read a line
# of text and determine whether or not the text is a palindrome.

.text
main:
	# prompt user to enter a string
	la $a0, prompt
	li $v0, 4
	syscall

	# receive user's input
	input: la $a0, string
	la $a1, 10000
	li $v0, 8
	syscall


	# get locations of first and last characters in string
	la $t0, string  # $t0 = reference to first character
	la $t1, string  # $t1 will ultimately reference last character

	loop1:
		addu $t1, $t1, 1  # increment $t1
		lb $t2, ($t1)  # get character at $t1

		# unless $t2 is an empty character (meaning it is
		# beyond the end of string), jump to top of loop1
		bnez $t2, loop1

	subu $t1, $t1, 2  # cut off empty characters at end of string

	# if user didn't enter a string, get a new line of input
	bgt $t0, $t1, input


	# test if string is a palindrome
	loop2:
		# if $t0 and $t1 meet in the middle of string (meaning all
		# characters have been compared), string is a palindrome
		bge $t0, $t1, isPalindrome

		# load values of characters at $t0 and $t1
		lb $t2, ($t0)  # forward-walking index from start of string
		lb $t3, ($t1)  # backward-walking index from end of string

		# increment $t0 and $t1 for next loop
		addu $t0, $t0, 1
		subu $t1, $t1, 1

		# if $t2 and $t3 are identical characters, continue looping
		beq $t2, $t3, loop2


	# run next 4 lines by default unless loop2 branched to isPalindrome
	la $a0, outputIfNotPalindrome
	li $v0, 4
	syscall
	j exit

	# the next 3 lines will run only if loop2 branched to them
	isPalindrome: la $a0, outputIfPalindrome
	li $v0, 4
	syscall


	# exit the program
	exit: li $v0, 10
	syscall



.data
	# reserve space for a string up to 10000 characters in length
	string: .space 10000

	prompt: .asciiz "Enter a string to determine whether it is a palindrome (spelled the same forward and backward):\n"
	outputIfPalindrome: .asciiz "\nYour string is a palindrome!"
	outputIfNotPalindrome: .asciiz "\nYour string is not a palindrome."